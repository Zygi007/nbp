<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = ['currency', 'code', 'mid'];
    protected $table = 'currencies';

    public function exchange()
    {
        return $this->belongsTo('App\Exchange');
    }
}
