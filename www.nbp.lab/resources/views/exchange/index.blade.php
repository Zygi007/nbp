@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Kurs walut - Tabela: {{$exchange->table}}
                        {{$exchange->no}}
                        Data: {{$exchange->effectiveDate}}</div>
                    <div class="card-body">
                        @foreach($exchange->currencies as $currency)
                            <ul class="list-inline">
                                <li class="list-inline-item">{{$currency->code}}</li>
                                <li class="list-inline-item">{{$currency->mid}}</li>
                                <li class="list-inline-item">{{$currency->currency}}</li>
                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
