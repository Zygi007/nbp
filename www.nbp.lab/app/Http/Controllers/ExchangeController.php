<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\Exchange;



class ExchangeController extends Controller
{

    public static function getExchange()
    {
        try {
            $client = new Client();
            $response = $client->request('GET', 'http://api.nbp.pl/api/exchangerates/tables/A/?format=json');

            $response = $response->getBody()->getContents();
            $response = json_decode($response, true);

            foreach ($response as $exchanges) {
                $exchange = Exchange::create($exchanges);
                if (is_array($exchanges['rates'])) {
                    foreach ($exchanges['rates'] as $currencyItem) {
                        $exchange->currencies()->create($currencyItem);
                    }
                } else {
                    throw new Exception('Not Currencies');
                }
            }
            return true;
        } catch (RequestException $e){
            return $e->getResponse()->getStatusCode();
        }
        return false;
    }

    public function index()
    {
        $exchange = Exchange::with('currencies')->latest()->first();
        return view('exchange.index', compact('exchange'));
    }
}
