<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    protected $fillable = ['table', 'no', 'effectiveDate'];
    protected $table = 'exchanges';

    public function currencies()
    {
        return $this->hasMany('App\Currency');
    }
}
