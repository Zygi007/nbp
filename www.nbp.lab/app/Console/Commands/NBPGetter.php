<?php

namespace App\Console\Commands;

use App\Http\Controllers\ExchangeController;
use Illuminate\Console\Command;

class NBPGetter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nbp:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get NBP exchange rate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (true === ExchangeController::getExchange()){
            $this->info('NBP:Cron Command Run successfully!');
        } else {
            $this->info('NBP:Cron Command Run Error!');
        }
    }
}
